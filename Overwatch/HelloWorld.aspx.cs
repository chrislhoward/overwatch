﻿using System;
using IBM.Data.DB2.EntityFramework;
using System.Data.Common;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Overwatch {
    public partial class HelloWorld : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs args) {
            String MyDb2ConnectionString = "database=TRERP;uid=appint;pwd=ap01ap;";

            // Use the dsn alias that you defined in db2dsdriver.cfg with the db2cli writecfg command in step 1.
            DB2ConnectionFactory db2ConnectionFactory = new DB2ConnectionFactory();

            DbConnection MyDb2Connection = db2ConnectionFactory.CreateConnection(MyDb2ConnectionString);
            MyDb2Connection.Open();

            DbCommand MyDB2Command = MyDb2Connection.CreateCommand();
            MyDB2Command.CommandText = "select count(*) as rowCount from $filebdm4.wm440m";

            Console.WriteLine(MyDB2Command.CommandText);

            DbDataReader MyDb2DataReader = MyDB2Command.ExecuteReader();
            Console.WriteLine("Data");
            Console.WriteLine("============================");
            while (MyDb2DataReader.Read()) {
                for (int i = 0; i <= 1; i++) {
                    try {
                        if (MyDb2DataReader.IsDBNull(i)) {
                            Console.Write("NULL");
                        } else {
                            Console.Write(MyDb2DataReader.GetString(i));
                        }
                    } catch (Exception e) {
                        Console.Write(e.ToString());
                    }
                    Console.Write("\t");

                }
                Console.WriteLine("");
            }
            MyDb2DataReader.Close();
            MyDB2Command.Dispose();
            MyDb2Connection.Close();
        }
    }
}